## Development environment

How to install the development environment:

```
git clone <.git>
cd <dir>
mkdir env | virtualenv  -p python3 --no-site-packages env --prompt="[futuur-api]"
source env/bin/activate
pip install -r requirements.txt
```

To run:

```
honcho -f Procfile.dev start web
```