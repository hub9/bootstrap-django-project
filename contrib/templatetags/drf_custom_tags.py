from django import template
from django.urls.base import reverse
from rest_framework.renderers import HTMLFormRenderer

register = template.Library()

@register.simple_tag
def drf_field(field):
    style = {'template_pack': 'rest_framework/custom/'}
    renderer = style.get('renderer', HTMLFormRenderer())
    return renderer.render_field(field, style)

@register.simple_tag
def drf_form_url(url_base_name, instance_id=None):
    url_suffix = '-list'
    if instance_id:
        url_suffix = '-detail'
    url_name = url_base_name + url_suffix
    args = None
    if instance_id:
        args = (instance_id,)
    return reverse(url_name, args=args)

@register.simple_tag
def drf_form_method(instance_id=None):
    return "POST" if not instance_id else "PATCH"