from django.conf import settings
from storages.backends.s3boto import S3BotoStorage


class MediaStorage(S3BotoStorage):
    bucket_name = settings.AWS_MEDIA_STORAGE_BUCKET_NAME

    def path(self, name):
        # From http://www.laplacesdemon.com/2013/07/11/solving-django-storage-notimplementederror-for-amazon-s3/
        return self._normalize_name(name)
