from django.shortcuts import render
from django.views.generic.detail import DetailView


class DrfDetailView(DetailView):
    """
    This View can be user both for creation and edition of objects.

    eg.:
    url(r'^foo/create/$', FooDetailView.as_view(), name='foo-create'),
    url(r'^foo/(?P<pk>[0-9]+)/$', FooDetailView.as_view(), name='foo-detail'),
    """
    def get(self, request, *args, **kwargs):
        instance = None
        if self.kwargs.get(self.pk_url_kwarg) or self.kwargs.get(self.slug_url_kwarg):
            instance = self.get_object()
        serializer = self.serializer_class(instance=instance)
        ctx = {'serializer': serializer, 'instance': instance}
        return render(request, self.template_name, ctx)