from __future__ import unicode_literals
import operator
from collections import OrderedDict
from functools import reduce
from rest_framework import filters
from django.db import models
from rest_framework.settings import api_settings


class FilterUserOwner(filters.DjangoFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return super().filter_queryset(request, queryset.filter(user=request.user), view)


class FullWordSearchFilter(filters.BaseFilterBackend):
    # Adapted from https://github.com/trollknurr/django-rest-framework-word-search-filter

    # The URL query parameter used for the search.
    search_param = api_settings.SEARCH_PARAM
    field_lookup_map = [
        ("{}__icontains", " {} "),
        ("{}__istartswith", "{} "),
        ("{}__iendswith", " {}"),
        ("{}__exact", "{}"),
        ("{}__istartswith", "{}-"),
        ("{}__iendswith", "-{}"),
        ("{}__icontains", "-{} "),
        ("{}__icontains", " {}-")
    ]

    def get_search_terms(self, request):
        """
        Search terms are set by a ?search=... query parameter,
        and may be comma and/or whitespace delimited.
        """
        params = request.query_params.get(self.search_param, '')
        return params.replace(',', ' ').split()

    def construct_search(self, field_name):
        return [key.format(field_name) for key, value in self.field_lookup_map]

    def construct_term(self, term):
        return [value.format(term) for key, value in self.field_lookup_map]

    def filter_queryset(self, request, queryset, view):
        search_fields = getattr(view, 'word_fields', None)

        if not search_fields:
            return queryset

        orm_lookups = [self.construct_search(str(search_field))
                       for search_field in search_fields]

        search_term = request.query_params.get(self.search_param, '').split()

        if not search_term:
            return queryset

        lookup_list = list()
        for orm_lookup in orm_lookups:
            and_query = list()
            for term in search_term:
                if term.startswith('-') or term.endswith('-'):
                    term = term.replace('-', '')
                and_query.append(
                    reduce(operator.or_, [models.Q(**{lookup: prep_term}) for lookup, prep_term in zip(orm_lookup, self.construct_term(term))]))
            lookup_list.append(reduce(operator.and_, and_query))
        queryset = queryset.filter(reduce(operator.or_, lookup_list))
        return queryset


class WordSearchFilter(FullWordSearchFilter):
    field_lookup_map = [
        ("{}__icontains", "{}")
    ]
