from collections import OrderedDict
from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.LimitOffsetPagination):
    page_size_query_param = 'limit'
    page_size = 20
    max_page_size = 20

    def get_paginated_data(self, data):
        return OrderedDict([
            ('pagination', {
                'total': self.count,
                'next': self.get_next_link(),
                'previous': self.get_previous_link(),
                'page_size': self.page_size,
                'offset': self.offset,
            }),
            ('results', data)
        ])

    def get_paginated_response(self, data):
        return Response(self.get_paginated_data(data))
