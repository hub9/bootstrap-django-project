from moneyed import Money
from rest_framework import serializers


class MoneyField(serializers.Field):
    def to_representation(self, obj):
        return obj.amount

    def to_internal_value(self, data):
        return Money(data['amount'], data['currency'])
