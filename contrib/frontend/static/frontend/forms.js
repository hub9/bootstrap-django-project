/*
 * Serializes a form into an object containing it's fields
 * Taken from: http://stackoverflow.com/a/1186309/1459749
 * eg.: $('form').serializeObject()
 * LAST UPDATE: 2016/04/06 - @rafael-vieira
 * Contributors: @pabdelhay and @rafael-vieira
 */
$.fn.serializeObject = function()
{
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

var getCookie = function(c_name){
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
};
var clearForm = function($form){
	clearErrors($form);
	$form.find('input[type="text"], select, textarea').val("");
};
var clearErrors = function($form){
	$form.find('input, select, textarea').removeClass('error');
	$form.find('.error-msg-all').html("").hide();
	$form.find('.error-msg').remove();
};
var renderErrors = function($form, errors_dict){
	clearErrors($form);
	$.each(errors_dict, function(field, errors){
		var showError = function($element, msg){
			var error_msg = msg;
			var error_html = '<div class="error-msg">' + error_msg + '</div>';
			var $field_element = $element;
			var $next = $field_element.next();
			var $parent = $field_element.parent();
			$field_element.addClass('error');

			if (!$field_element.is(':visible') && $next.hasClass('chosen-container')){
				// Special treatment for chosen selects.
				$next.after(error_html);
			} else if ($parent.hasClass('input-group')) {
				// Special treatment for text inputs within input groups.
				$parent.after(error_html);
			} else {
				$field_element.after(error_html);
			}
		}

		var error_msg = "";
		if($.isArray(errors)){
			if(errors.length == 0)
				return; // Skip empty arrays.

			var idx = -1;
			$.each(errors, function(_, error){
				// For nested fields
				idx++;
				if($.isPlainObject(error)){
					if($.isEmptyObject(error)){
						return; // continue
					}

					for(k in error){
						var msg = $.isArray(error[k]) ? error[k].join('<br />') : error[k];
						var $field_php = $('[name="' + field+'[]['+k+']' +'"]').eq(idx);
						var $field_django_formset = $('[name="' + field+'-'+idx+'-'+k+'"]');
						// Search for fields in both php and django formset standards.
						showError($field_php, msg);
						showError($field_django_formset, msg);
					}
				} else {
					error_msg += error + '<br />';
				}
			});
		} else {
			error_msg = errors;
		}

		if(field == '__all__' || field == 'non_field_errors'){
			var $error_msg_all_wrapper = $form.find('.error-msg-all');
			if($error_msg_all_wrapper.length > 0){
				$error_msg_all_wrapper.html(error_msg).show();
			} else {
				var error_html = '<div class="error-msg error-msg-all">' + error_msg + '</div>';
				$form.append(error_html);
			}
		} else {
			showError($form.find('[name="' + field + '"]'), error_msg);
		}

	});
};
var prepareCheckboxes = function($form){
	$form.find('[type="checkbox"]').each(function(){
		if($(this).val() == "on")
			$(this).val("True");
	});
}

if(typeof loading !== 'function') {
	var loading = function(show){
		var show = typeof show === 'undefined' ? true : false;
		if(show){
			$('body').prepend('<div class="ajaxForm-loading" style="position: fixed; background-color: #FFF; z-index: 99999; top: 0; left: 0; padding: 10px">Loading...</div>');
		} else {
			$('.ajaxForm-loading').remove();
		}
	};
}

( function($) {
	$.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
	var methods = {

		submit: function() {
			return this.trigger('submit');
		},

		init: function(options) {
			var $form = this;
			prepareCheckboxes($form);

			// Default callbacks.
			var success_callback = function() {
				alert("Success");
			};
			var error_callback = function(errors) {
				renderErrors($form, errors);
			};
			var prepare_data_callback = function(data) {
				return data;
			};

			var settings = $.extend({
				// These are the defaults.
				url : null, // (url) URL of the page to send the form via ajax.
				method : null, // (string) Method of ajax request. 'POST' (default), 'PUT', 'GET' or 'DELETE'.
				serialize_data : null, // (bool) If true serializes data into a unique value of an object. eg.: {'form': serialized_data}.
				on_success : success_callback, // (function) On success callback. Called if response has NO key 'errors' or it's value is empty.
				on_error : error_callback, // (function) On error callback. Called if response has key 'errors' and it's value is not empty.
				prepare_data : null, // (function) Manipulate data before sends to the server. The method must receive an object and return the new data as another object. 
				show_loading : true // (bool) If true, calls 'loading()' function to show a loading div while sending data to server.
			}, options);

			// Check for existance or either url or dajax_view param. If none of them is
			// passed, consider the form's actions as url.
			if(!settings.url) {
				if($form.attr('action')){
					settings.url = $form.attr('action');
				} else {
					console.error("ajaxForm: you must pass 'url' as an option OR set the 'action' attribute to the form.");
					return false;
				}
			}
			// If serialize_data is not explicity passed, set it to false.
			if(settings.serialize_data === null){
				settings.serialize_data = false;
			}
			// Check for method. Default behaviour is to set 'POST', but it can be override by options.
			// If there is any not null field named 'id' OR 'pk' in the form, the method is set to 'PUT'.


			if(settings.method === null){
				if($form.attr('method')){
					settings.method = $form.attr('method');
				} else {
					if($form.find('[name="id"]').val() || $form.find('[name="pk"]').val()){
						settings.method = 'PUT';
					} else {
						settings.method = 'POST';
					}
				}
			}


			function submitForm(ev) {
				ev.preventDefault();

				// Clear errors first
				clearErrors($form);

				// Shows loading
				if(settings.show_loading){
					loading();
				}

				// Prepare data to send to the server
                var data = new FormData(this);
                if (settings.prepare_data !== null) {
                    var custom_data = $form.serializeObject();
                    custom_data = settings.prepare_data( custom_data );
                    $.each(custom_data, function(k, v){
                        data.set(k,v);
                    });
                }
                
				// Serializes data into an object containing {'form': serialized_form}
				if(settings.serialize_data) {
					data = {'form': data};
				}

				if(settings.url) {
					// Sends traditional way via ajax
					$.ajax(settings.url, {
						type: settings.method,
						data: data,
                        processData: false,
                        contentType: false
					}).done(function(data, textStatus, jqXHR){
						if(!$.isEmptyObject(data.errors)){
							settings.on_error.call( $form, data );
						} else {
							settings.on_success.call( $form, data );
						}
					}).fail(function(jqXHR, textStatus, errorThrown){
						if(jqXHR.status == 400){  // Default api response when there is a validation error.
							settings.on_error.call( $form, jqXHR.responseJSON );
						} else if(jqXHR.status == 403) {  // Forbidden.
							var msg = jqXHR.status + " (" + jqXHR.statusText + ") - " + jqXHR.responseText;
							console.log(msg);
							alert("Você não tem permissão para executar esta ação.");
						} else {  // Misterious error.
							var msg = jqXHR.status + " (" + jqXHR.statusText + ") - " + jqXHR.responseText;
							console.log(msg);
							alert("Ocorreu um erro na solicitação: " + msg);
						}
					}).always(function(){
						if(settings.show_loading){
							loading(false);
						}
					});
				}
			} // end submitForm()

			/*
			 * Binds
			 */
			$form.bind('submit', submitForm)
			//$(document).on('submit', $form, submitForm);

			return this;
		}

	};

	$.fn.ajaxForm = function(method) {
		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.ajaxForm' );
		}
	};
}(jQuery));
