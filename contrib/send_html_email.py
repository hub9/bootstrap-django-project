# -*- coding: utf-8 -*-
import os

from django.conf import settings
from django.core.mail.message import EmailMultiAlternatives
from django.template.context import Context
from django.template.loader import render_to_string
from django.utils.html import strip_tags


class EmailContext(Context):
    """
    A context that contains basic settings data in the same way that
    RequestContext does, but without requiring a request object to be passed,
    so it can be used in methods that don't have access to the request.
    """

    def __init__(self):
        super(EmailContext, self).__init__()
        data = {
            'STATIC_URL': settings.STATIC_URL,
            'MEDIA_URL': settings.MEDIA_URL,
            'SITE_URL': settings.SITE_URL,
            'STYLE': {
                'body': 'font-size: 14px;',
                'box': 'background-color: #F3F2FF; color: #333; padding: 10px; font-size: 15px; border: 1px solid #00406D; max-width: 600px;',
                'btn_green': 'background-color: #48CA3B; color: #FFF; font-size: 14px; padding: 15px; text-decoration: none; font-weight: bold; border-radius: 5px;',
                'btn_red': 'background-color: #AD1D28; color: #FFF; font-size: 14px; padding: 15px; text-decoration: none; font-weight: bold; border-radius: 5px;',
                'btn_yellow': 'background-color: #C9C337; color: #FFF; font-size: 14px; padding: 15px; text-decoration: none; font-weight: bold; border-radius: 5px;',
                'btn_default': 'background-color: #2F72A1; color: #FFF; font-size: 14px; padding: 15px; text-decoration: none; font-weight: bold; border-radius: 5px;',
                'margin_top': 'margin-top: 20px;',
                'margin_bottom': 'margin-bottom: 20px;',
                'actions': 'margin-top: 30px;',
                'red': 'color: #AD1D28;',
                'green': 'color: #48CA3B;',
                'sec_info': 'font-size: 12px; color: #9E9E9E;',
                'featured': 'font-size: 16px; font-weight: bold;',
                'big_text': 'font-size: 24px;',
                'text_bigger': 'font-size: 125%;',
                'text_smaller': 'font-size: 75%;',
                'panel': 'border-radius: 5px; border: 1px solid #CCC; font-size: 18px; margin: 5px;',
                'panel_header': 'padding: 10px;',
                'panel_header_primary': 'background-color: #428BCA; color: #FFF;',
                'panel_header_red': 'background-color: #D9534F; color: #FFF;',
                'panel_header_yellow': 'background-color: #F0AD4E; color: #FFF;',
                'panel_header_green': 'background-color: #5CB85C; color: #FFF;',
                'panel_body': 'padding: 10px;',
                'pre': 'background-color: #f1f1f1; padding: 10px;'
            }
        }
        self.update(data)


def send_html_email(template, to, subject, context={}, from_email=None, context_instance=None,
                   apply_subject_prefix=True, attachment=None, attachment_filename=None):
    """
    Sends HTML email.

    Args:
        template: string
        to: [(name:string, email:string)] or [email:string]
        subject: string
        context: dict
        from_email: email:string
        context_instance: context
        apply_subject_prefix: bool
        attachment: File
        attachment_filename: string
    Returns: None
    """
    context_instance = context_instance or EmailContext()
    from_email = from_email or settings.DEFAULT_FROM_EMAIL
    subject = settings.EMAIL_SUBJECT_PREFIX + subject if apply_subject_prefix else subject
    to_addresses = []
    for address in to:
        if not address:
            continue
        if isinstance(address, (list, tuple)):
            if len(address) == 1:
                email = address[0]
                if not email:
                    continue
                to_addresses.append("<%s>" % (email,))
            else:
                name = address[0]
                email = address[1]
                if not email:
                    continue
                to_addresses.append("%s <%s>" % (name, email,))
        else:
            to_addresses.append("<%s>" % (address,))

    html_message = render_to_string(template, context, context_instance)
    text_content = strip_tags(html_message)  # this strips the html, so people will have the text as well.

    msg = EmailMultiAlternatives(subject, text_content, from_email, to_addresses)
    msg.attach_alternative(html_message, "text/html")
    if attachment:
        filename = attachment_filename or os.path.basename(attachment.name)
        msg.attach(filename=filename, content=attachment.read())
    msg.send()
