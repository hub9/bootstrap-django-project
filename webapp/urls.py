import debug_toolbar
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin

import playground.urls
from contrib.rest_framework.hybridrouter import HybridRouter
from playground.urls import foo_router

router = HybridRouter()
router.register_router(foo_router)

urlpatterns = [
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    url(r'^api/', include(router.urls)),
    url(r'^{}/'.format(settings.ADMIN_SITE_PREFIX), admin.site.urls),
    url(r'', include(playground.urls)),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
