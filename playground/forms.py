from django import forms

from playground.models.foo import Foo


class FooForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Foo