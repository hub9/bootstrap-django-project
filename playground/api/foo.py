from rest_framework import serializers, viewsets
from playground.models.foo import Foo


class FooSerializer(serializers.ModelSerializer):
    class Meta:
        model = Foo
        fields = '__all__'


class FooViewset(viewsets.ModelViewSet):
    model = Foo
    queryset = Foo.objects.all()
    serializer_class = FooSerializer
    permission_classes = []
    filter_backends = []
