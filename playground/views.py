from django.shortcuts import render
from django.views.generic.list import ListView

from contrib.views import DrfDetailView
from playground.api.foo import FooSerializer
from playground.models.foo import Foo


class FooListView(ListView):
    queryset = Foo.objects.all()
    template_name = 'playground/form.html'


class FooDetailView(DrfDetailView):
    queryset = Foo.objects.all()
    serializer_class = FooSerializer
    template_name = 'playground/form.html'



def form(request):
    ctx = {}
    instance = Foo.objects.get(id=1)
    ctx['instance'] = instance
    ctx['serializer'] = FooSerializer(instance=instance)
    return render(request, 'playground/form.html', ctx)


def listing(request):
    ctx = {}
    ctx['items'] = Foo.objects.all()
    return render(request, 'playground/listing.html', ctx)
