# -*- coding: utf-8 -*-
from django.conf.urls import url
from contrib.rest_framework.hybridrouter import HybridRouter
from playground.api.foo import FooViewset
from playground.views import FooDetailView, FooListView
from . import views

urlpatterns = [
    url(r'^foo/$', FooListView.as_view(), name='foo-list'),
    url(r'^foo/create/$', FooDetailView.as_view(), name='foo-create'),
    url(r'^foo/(?P<pk>[0-9]+)/$', FooDetailView.as_view(), name='foo-detail'),

    url(r'^form/', views.form, name='sample-form'),
    url(r'^listing/', views.listing, name='sample-listing'),
]

foo_router = HybridRouter()
foo_router.register(r'foo', FooViewset, base_name='api-foo')
