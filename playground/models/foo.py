from django.contrib import admin
from django.db import models


class Foo(models.Model):
    name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    @classmethod
    def register_admin(cls):
        admin.site.register(cls, Admin)


class Admin(admin.ModelAdmin):
    pass