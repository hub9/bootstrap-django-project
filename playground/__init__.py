from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'playground'
    verbose_name = "playground"

default_app_config = 'playground.AppConfig'
