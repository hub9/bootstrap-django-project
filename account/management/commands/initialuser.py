from account.models.user import User
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    help = "Creates an admin user if no users are present in the current database."

    def handle(self, *args, **options):
        # Skip django-guardian AnonymousUser that is created by the initial migrations.
        if not User.objects.exclude(email__iexact='AnonymousUser').count():
            try:
                user = User.objects.create(
                    email="admin@example.org",
                    first_name="Admin",
                    last_name="Admin",
                    is_staff=True,
                    is_superuser=True,
                    is_active=True
                )
                user.set_password("admin")
                user.save()
            except:
                raise CommandError("Couldn't create initial admin user.")

            self.stdout.write(self.style.SUCCESS("Initial admin user successfully created."))
