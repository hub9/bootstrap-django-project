from django.db.models.aggregates import Sum
from django.db.models.expressions import F
from django.db.models.fields import DecimalField
from rest_framework import permissions, serializers, viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

from account.models.user import User
from wager.models.wager import Wager
from wager.models.wager_purchase import WagerPurchase
from wager.models.wager_sale import WagerSale


class UserSummarySerializer(serializers.ModelSerializer):
    balance_amount = serializers.SerializerMethodField()
    balance_currency = serializers.SerializerMethodField()
    total_shares = serializers.SerializerMethodField()
    total_shares_paid = serializers.SerializerMethodField()
    shares_current_worth = serializers.SerializerMethodField()
    net_worth = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'gender',
                  'balance_amount', 'balance_currency', 'total_shares', 'total_shares_paid', 'shares_current_worth',
                  'net_worth')

    def get_balance_amount(self, obj):
        return obj.account.balance.amount

    def get_balance_currency(self, obj):
        return obj.account.balance.currency.name

    def get_total_shares(self, obj):
        return Wager.objects.filter(user=obj, status=Wager.STATUS_CHOICES.purchased)\
            .aggregate(total=(Sum('shares')))['total'] or 0

    def get_total_shares_paid(self, obj):
        # TODO: To be changed when partial sale is possible.
        purchased = WagerPurchase.objects.filter(wager__user=obj, wager__status=Wager.STATUS_CHOICES.purchased)\
            .aggregate(total=(Sum(F('price')*F('shares'), output_field=DecimalField())))['total'] or 0
        sold = WagerSale.objects.filter(wager__user=obj, wager__status=Wager.STATUS_CHOICES.sold)\
            .aggregate(total=(Sum(F('price')*F('shares'), output_field=DecimalField())))['total'] or 0
        return purchased - sold

    def get_shares_current_worth(self, obj):
        # TODO: To be changed when partial sale is possible.
        return Wager.objects.filter(user=obj, status=Wager.STATUS_CHOICES.purchased)\
            .aggregate(total=Sum(F('outcome__current_price')*F('shares'), output_field=DecimalField()))['total']\
               or 0

    def get_net_worth(self, obj):
        return self.get_shares_current_worth(obj) + self.get_balance_amount(obj)


class MeViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSummarySerializer
    permission_classes = (permissions.IsAuthenticated,)
    pagination_class = None

    def get_object(self):
        return self.request.user

    def list(self, request, *args, **kwargs):
        user = self.get_object()
        return Response(self.serializer_class(user).data)

    @list_route(methods=['get'])
    def summary(self, request):
        user = self.get_object()
        return Response(UserSummarySerializer(user).data)

    @list_route(methods=['get'])
    def balance(self, request):
        user = self.get_object()
        return Response({'amount': user.account.balance.amount, 'currency': user.account.balance.currency.name})
