import json

from django.contrib.auth.hashers import make_password
from rest_framework.response import Response
from rest_framework import viewsets, mixins, status, serializers
from account.models.user import User
from contrib.rest_framework.permissions import IsAuthenticatedOrCreate


class UserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        username = validated_data.get('username', User.generate_username())
        user = User(
            username=username,
            email=validated_data['email'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
            gender=validated_data['gender'],
            birth_date=validated_data['birth_date']
        )
        user.set_password(validated_data['password'])
        user.save()

        return user

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'username', 'password', 'gender', 'birth_date')
        extra_kwargs = {
            'password': {'write_only': True},
            'email': {'write_only': True},
        }


class UserViewSet(viewsets.ReadOnlyModelViewSet, mixins.CreateModelMixin, mixins.UpdateModelMixin):
    queryset = User.objects.filter(is_active=True)
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticatedOrCreate, )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response = serializer.data
        user = serializer.instance
        response['oauth'] = user.get_access_token()
        return Response(response, status=status.HTTP_201_CREATED, headers=headers)

    def perform_update(self, serializer):
        if 'password' in serializer.validated_data:
            serializer.validated_data['password'] = make_password(serializer.validated_data['password'])
        serializer.save()

