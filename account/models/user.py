from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _

from contrib.send_html_email import send_html_email


class User(AbstractUser):
    username = models.CharField(_('username'), max_length=150, blank=True)
    email = models.EmailField("email", unique=True, error_messages={'unique': _("This email is already registered.")})
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'username']

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "users"

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        is_new = not self.pk
        if not self.username:
            self.username = self.email[:150]
        super(User, self).save(*args, **kwargs)
        # if is_new:
        #     send_html_email(template='account/email_user_registration.html', to=[self.email],
        #                    subject="Welcome to Futuur", context={'user': self})

    @classmethod
    def register_admin(cls):
        admin.site.register(cls, Admin)


class Admin(UserAdmin):
    search_fields = ('email', 'first_name', 'last_name')
    list_display = ('email', 'first_name', 'last_name', 'is_staff')
    list_filter = ('is_active', 'is_staff', 'is_superuser')
    ordering = ('email',)
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ("Personal information", {'fields': ('first_name', 'last_name',)}),
        ("Permissions", {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ("Activity", {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
