from datetime import datetime


def save_extra(backend, user, response, *args, **kwargs):
    # TODO: Review this method.
    if backend.name == 'facebook':
        try:
            user.gender = response.get('gender')[0]  # Only get the first letter as in 'male' and 'female'
            user.birth_date = datetime.strptime(response.get('birthday'), '%m/%d/%Y')
        except ValueError:
            pass
        user.save()
