from django.apps import AppConfig as BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'account'
    verbose_name = "Users"

default_app_config = 'account.AppConfig'
