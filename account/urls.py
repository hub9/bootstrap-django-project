from account.api import me, user
from contrib.rest_framework.hybridrouter import HybridRouter

router = HybridRouter()
router.register(r'user', user.UserViewSet)
router.register(r'me', me.MeViewSet)
